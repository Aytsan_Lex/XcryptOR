#include "mainwindow.hpp"

#include <QApplication>

#ifdef _WIN32
#include <windows.h>
int APIENTRY WinMain(HINSTANCE hInst, HINSTANCE hInstPrev, PSTR cmdline, int cmdshow)
{
  int argc {0};
  char* argv {reinterpret_cast<char*>(CommandLineToArgvW(reinterpret_cast<LPCWSTR>(cmdline), &argc))};

  QApplication app {argc, &argv};
  MainWindow main_window {};
  main_window.show();
  return app.exec();
}
#else
int main(int argc, char** argv)
{
  QApplication app {argc, argv};
  MainWindow main_window {};
  main_window.show();
  return app.exec();
}
#endif // !_WIN32

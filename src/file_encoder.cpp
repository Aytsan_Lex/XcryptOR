#include <QFileInfo>

#include "file_encoder.hpp"

FileEncoder::FileEncoder(QObject* parent)
  : QObject {parent}
  , input_file_ {}
  , output_file_ {}
{ }

FileEncoder::~FileEncoder()
{
  input_file_.close();
  output_file_.close();
}

bool FileEncoder::open(const QString& input_filename)
{
  input_file_.setFileName(input_filename);
  input_file_.open(QIODevice::ReadOnly);
  return input_file_.isOpen();
}

void FileEncoder::close()
{
  input_file_.close();
}

void FileEncoder::set_output_file_option(OutputOption option)
{
  output_file_op_ = option;
}

void FileEncoder::encode(uint64_t code)
{
  QByteArray file_data {input_file_.readAll()};
  std::for_each(file_data.begin(), file_data.end(), [code](auto& byte) { byte ^= code; });

  output_file_.setFileName(generate_output_filename());
  write_data_to_output(file_data);
}

FileEncoder::OutputOption FileEncoder::get_output_file_option() const noexcept
{
  return output_file_op_;
}

QString FileEncoder::generate_output_filename() const
{
  const QString base_filename {QFileInfo{input_file_}.baseName()};
  const QString file_path {QFileInfo{input_file_}.path()};

  QString filename {QStringLiteral("%1/%2.bin").arg(file_path, base_filename)};

  if (output_file_op_ == OutputOption::create_new)
  {
    int counter {1};
    while (QFileInfo::exists(filename))
    {
      filename = QStringLiteral("%1/%2.bin.%3").arg(file_path, base_filename, QString::number(counter));
      counter++;
    }
  }

  return filename;
}

void FileEncoder::write_data_to_output(const QByteArray& data)
{
  if (output_file_op_ == OutputOption::overwrite)
  {
    output_file_.open(QIODevice::WriteOnly | QIODevice::Truncate);
  }
  else
  {
    output_file_.open(QIODevice::WriteOnly);
  }

  if (output_file_.isOpen())
  {
    output_file_.write(data);
    output_file_.close();
  }

  emit encode_finished(output_file_.fileName());
}

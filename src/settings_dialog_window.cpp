#include "settings_dialog_window.hpp"
#include "./ui_settings_dialog_window.h"

SettingsDialogWindow::SettingsDialogWindow(QWidget* parent)
  : QDialog {parent}
  , ui {new Ui::SettingsDialogWindow}
{
  ui->setupUi(this);
  connect(this, &QDialog::accepted, this, &SettingsDialogWindow::on_accepted);
}

SettingsDialogWindow::~SettingsDialogWindow()
{
  delete ui;
}

void SettingsDialogWindow::set_settings(const nlohmann::json& settings)
{
  settings_ = settings;
}

const nlohmann::json& SettingsDialogWindow::get_settings() const noexcept
{
  return settings_;
}

void SettingsDialogWindow::on_accepted()
{
  settings_["delete_input_file"] = ui->checkBox_delete_input_file->isChecked();
  settings_["run_on_timer"] = ui->checkBox_run_on_timer->isChecked();
  settings_["timer_interval"] = ui->lineEdit_timer_interval->text().toUInt();
  settings_["input_file_path"] = ui->lineEdit_input_file_path->text().toStdString();
  settings_["output_file_path"] = ui->lineEdit_output_file_path->text().toStdString();
  settings_["input_file_mask"] = ui->lineEdit_input_file_mask->text().toStdString();
}

void SettingsDialogWindow::on_toolButton_open_input_path_clicked()
{
  const auto path {QFileDialog::getExistingDirectory(this,
                                                     QStringLiteral("Choose input file directory"))};
  if (!path.isEmpty())
  {
    ui->lineEdit_input_file_path->setText(path);
  }
}

void SettingsDialogWindow::on_toolButton_open_output_path_clicked()
{
  const auto path {QFileDialog::getExistingDirectory(this,
                                                     QStringLiteral("Choose output file directory"))};
  if (!path.isEmpty())
  {
    ui->lineEdit_output_file_path->setText(path);
  }
}

void SettingsDialogWindow::showEvent(QShowEvent* evt)
{
  ui->checkBox_delete_input_file->setChecked(settings_["delete_input_file"].get<bool>());
  ui->checkBox_run_on_timer->setChecked(settings_["run_on_timer"].get<bool>());
  ui->lineEdit_timer_interval->setText(QString::number(settings_["timer_interval"].get<int>()));
  ui->lineEdit_input_file_path->setText(QString::fromStdString(settings_["input_file_path"].get<std::string>()));
  ui->lineEdit_output_file_path->setText(QString::fromStdString(settings_["output_file_path"].get<std::string>()));
  ui->lineEdit_input_file_mask->setText(QString::fromStdString(settings_["input_file_mask"].get<std::string>()));
  evt->accept();
}

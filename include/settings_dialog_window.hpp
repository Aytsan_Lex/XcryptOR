#pragma once

#include <QDialog>
#include <QFileDialog>
#include <QShowEvent>

#include "nlohmann/json.hpp"

namespace Ui { class SettingsDialogWindow; }

class SettingsDialogWindow : public QDialog
{
  Q_OBJECT

public:
  explicit SettingsDialogWindow(QWidget* parent = nullptr);
  ~SettingsDialogWindow();

  void set_settings(const nlohmann::json& settings);
  const nlohmann::json& get_settings() const noexcept;

private slots:
  void on_accepted();
  void on_toolButton_open_input_path_clicked();
  void on_toolButton_open_output_path_clicked();

private:
  void showEvent(QShowEvent* evt) override;

private:
  Ui::SettingsDialogWindow* ui;
  nlohmann::json settings_;
};


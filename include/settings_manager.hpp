#pragma once

#include <QCoreApplication>
#include <QFile>

#include "nlohmann/json.hpp"

class SettingsManager
{
public:
  SettingsManager();
  ~SettingsManager();

  void set_filename(const QString& filename);
  void create_default_settings_file();

  nlohmann::json read();
  void write(const nlohmann::json& settings);

  static SettingsManager& instance() noexcept;
  static const nlohmann::json& default_settings();

private:
  QFile settings_file_;
};

#pragma once

#include <QMainWindow>
#include <QFileDialog>

#include "file_encoder.hpp"
#include "settings_dialog_window.hpp"
#include "settings_manager.hpp"
#include "input_file_detector.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT

  enum
  {
    MAX_HEX_DIGITS = 16,
    STATUSBAR_MESSAGE_TIMEOUT = 3000 // ms
  };

public:
  MainWindow(QWidget* parent = nullptr);
  ~MainWindow();

private slots:
  void on_action_Open_triggered();
  void on_action_Close_triggered();
  void on_action_Exit_triggered();
  void on_action_Settings_triggered();

  void on_pushButton_open_file_clicked();
  void on_pushButton_encode_clicked();
  void on_comboBox_option_activated(int index);

  void on_lineEdit_code_textChanged(const QString& text);

  void settings_accepted();
  void input_file_detected(const QString& filename);

private:
  void load_and_apply_settings();
  void apply_settings(const nlohmann::json& settings);
  void connect_signals();

private:
  Ui::MainWindow* ui;
  QString selected_input_filename_;

  FileEncoder file_encoder_;
  InputFileDetector file_detector_;

  SettingsDialogWindow settings_dialog_window_;

  bool is_run_on_timer_;
  bool delete_input_file_;
  QString input_file_mask_;
};

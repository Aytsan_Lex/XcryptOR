#pragma once

#include <QObject>
#include <QFile>
#include <QUrl>

class FileEncoder : public QObject
{
  Q_OBJECT

public:
  enum class OutputOption
  {
    overwrite,
    create_new
  };

  FileEncoder(QObject* parent = nullptr);
  ~FileEncoder();

public:
  bool open(const QString& input_filename);
  void close();

  void set_output_file_option(OutputOption option);

  void encode(uint64_t code);

  OutputOption get_output_file_option() const noexcept;

private:
  QString generate_output_filename() const;
  void write_data_to_output(const QByteArray& data);

signals:
  void encode_finished(const QString& filename);

private:
  QFile input_file_;
  QFile output_file_;

  OutputOption output_file_op_;
};

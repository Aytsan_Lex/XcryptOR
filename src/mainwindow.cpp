#include "mainwindow.hpp"
#include "./ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow {parent}
  , ui {new Ui::MainWindow}
  , selected_input_filename_ {}
  , file_encoder_ {this}
  , settings_dialog_window_ {this}
  , file_detector_ {this}
  , is_run_on_timer_ {false}
  , delete_input_file_ {false}
{
  ui->setupUi(this);

  load_and_apply_settings();
  connect_signals();

  file_encoder_.set_output_file_option(FileEncoder::OutputOption::overwrite);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_action_Open_triggered()
{
  ui->statusbar->showMessage(input_file_mask_, STATUSBAR_MESSAGE_TIMEOUT);

  selected_input_filename_ = QFileDialog::getOpenFileName(this,
                                                           QStringLiteral("Choose input file (%1)")
                                                            .arg(input_file_mask_),
                                                           {},
                                                           QStringLiteral("(%1)").arg(input_file_mask_));
  if (!selected_input_filename_.isEmpty())
  {
    if (file_encoder_.open(selected_input_filename_))
    {
      ui->lineEdit_input_filename->setText(selected_input_filename_);
      ui->pushButton_encode->setEnabled(true);
    }
  }
}

void MainWindow::on_action_Close_triggered()
{
  file_encoder_.close();
  selected_input_filename_.clear();
  ui->lineEdit_input_filename->clear();
}

void MainWindow::on_action_Exit_triggered()
{
  close();
}

void MainWindow::on_action_Settings_triggered()
{
  settings_dialog_window_.show();
}

void MainWindow::on_pushButton_open_file_clicked()
{
  on_action_Open_triggered();
}

void MainWindow::on_pushButton_encode_clicked()
{
  const QString text {ui->lineEdit_code->text()};

  if (text.isEmpty())
  {
    ui->statusbar->showMessage(QStringLiteral("Empty code!"), STATUSBAR_MESSAGE_TIMEOUT);
    return;
  }

  const uint64_t code {text.toULongLong(nullptr, 16)};
  file_encoder_.encode(code);

  if (delete_input_file_)
  {
    QFile::remove(selected_input_filename_);
    ui->statusbar->showMessage(QStringLiteral("Input file deleted"), STATUSBAR_MESSAGE_TIMEOUT);
  }
}

void MainWindow::on_comboBox_option_activated(int index)
{
  file_encoder_.set_output_file_option(static_cast<FileEncoder::OutputOption>(index));
}

void MainWindow::on_lineEdit_code_textChanged(const QString& text)
{
  if (text.length() > MAX_HEX_DIGITS)
  {
    ui->pushButton_encode->setEnabled(false);
    ui->statusbar->showMessage(QStringLiteral("Code is too long to be converted to uint64_t!"),
                               STATUSBAR_MESSAGE_TIMEOUT);
  }
  else if (!selected_input_filename_.isEmpty())
  {
    ui->pushButton_encode->setEnabled(true);
  }
}

void MainWindow::settings_accepted()
{
  const auto settings = settings_dialog_window_.get_settings();
  apply_settings(settings);
  SettingsManager::instance().write(settings);
}

void MainWindow::input_file_detected(const QString& filename)
{
  file_encoder_.open(filename);
  file_encoder_.encode(ui->lineEdit_code->text().toULongLong());
  file_encoder_.close();

  if (delete_input_file_)
  {
    QFile::remove(filename);
  }
}

void MainWindow::load_and_apply_settings()
{
  SettingsManager::instance().set_filename(QApplication::applicationDirPath() + "/settings.json");

  try
  {
    const auto settings = SettingsManager::instance().read();
    settings_dialog_window_.set_settings(settings);
    apply_settings(settings);
  }
  catch (const std::exception& err)
  {
    SettingsManager::instance().create_default_settings_file();
    settings_dialog_window_.set_settings(SettingsManager::default_settings());
    apply_settings(SettingsManager::default_settings());
  }
}

void MainWindow::apply_settings(const nlohmann::json& settings)
{
  is_run_on_timer_ = settings["run_on_timer"].get<bool>();
  delete_input_file_ = settings["delete_input_file"].get<bool>();
  input_file_mask_ = QString::fromStdString(settings["input_file_mask"].get<std::string>());

  if (is_run_on_timer_)
  {
    file_detector_.set_interval(settings["timer_interval"].get<int>());
    file_detector_.set_mask(input_file_mask_);
    file_detector_.set_path(QString::fromStdString(settings["input_file_path"].get<std::string>()));
    file_detector_.start();

    ui->lineEdit_code->setEnabled(false);
  }
  else
  {
    ui->lineEdit_code->setEnabled(true);
    file_detector_.stop();
  }
}

void MainWindow::connect_signals()
{
  connect(&file_encoder_, &FileEncoder::encode_finished, [this](const QString& filename) {
    ui->lineEdit_output_filename->setText(filename);
    ui->statusbar->showMessage("File encoded: " + filename, STATUSBAR_MESSAGE_TIMEOUT);
  });

  connect(&settings_dialog_window_, &QDialog::accepted, this, &MainWindow::settings_accepted);
  connect(&file_detector_, &InputFileDetector::detected, this, &MainWindow::input_file_detected);
}

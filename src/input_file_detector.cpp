#include "input_file_detector.hpp"

InputFileDetector::InputFileDetector(QObject* parent)
  : QObject {parent}
  , timer_ {}
  , path_ {}
{
  connect(&timer_, &QTimer::timeout, this, &InputFileDetector::on_timeout);
}

InputFileDetector::~InputFileDetector()
{ }

void InputFileDetector::set_path(const QString& path)
{
  path_.setPath(path);
}

void InputFileDetector::set_mask(const QString& mask)
{
  mask_ = mask;
}

void InputFileDetector::set_interval(int interval)
{
  timer_.setInterval(interval);
}

void InputFileDetector::start()
{
  timer_.start();
}

void InputFileDetector::stop()
{
  timer_.stop();
}

void InputFileDetector::on_timeout()
{
  const auto files {path_.entryList({mask_})};
  if (!files.empty())
  {
    emit detected(files[0]);
  }
}

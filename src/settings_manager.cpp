#include "settings_manager.hpp"

SettingsManager::SettingsManager()
  : settings_file_ {}
{ }

SettingsManager::~SettingsManager()
{ }

void SettingsManager::set_filename(const QString& filename)
{
  settings_file_.setFileName(filename);
}

void SettingsManager::create_default_settings_file()
{
  write(default_settings());
}

nlohmann::json SettingsManager::read()
{
  if (!settings_file_.open(QIODevice::ReadOnly))
  {
    throw std::runtime_error {"Failed to open settings file"};
  }

  const auto data {settings_file_.readAll().toStdString()};
  settings_file_.close();

  return nlohmann::json::parse(data);
}

void SettingsManager::write(const nlohmann::json& settings)
{
  if (!settings_file_.open(QIODevice::WriteOnly | QIODevice::Truncate))
  {
    throw std::runtime_error {"Failed to open settings file"};
  }

  const QByteArray data {QByteArray::fromStdString(settings.dump())};
  settings_file_.write(data);

  settings_file_.close();
}

SettingsManager& SettingsManager::instance() noexcept
{
  static SettingsManager settings_manager {};
  return settings_manager;
}

const nlohmann::json& SettingsManager::default_settings()
{
  static const std::string current_binary_dir {QCoreApplication::applicationDirPath().toStdString()};
  static const nlohmann::json settings {
    {"delete_input_file", false},
    {"run_on_timer", false},
    {"timer_interval", 1000},
    {"input_file_path", current_binary_dir},
    {"output_file_path", current_binary_dir},
    {"input_file_mask", "*.txt"},
  };
  return settings;
}

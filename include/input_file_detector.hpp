#pragma once

#include <QObject>
#include <QTimer>
#include <QDir>

class InputFileDetector : public QObject
{
  Q_OBJECT

public:
  InputFileDetector(QObject* parent = nullptr);
  ~InputFileDetector();

  void set_path(const QString& path);
  void set_mask(const QString& mask);
  void set_interval(int interval);

public slots:
  void start();
  void stop();

private slots:
  void on_timeout();

signals:
  void detected(const QString& file);

private:
  QTimer timer_;
  QDir path_;
  QString mask_;
};
